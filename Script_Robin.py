# Reading an excel file using Python 
import xlrd 
import xlwt
  
# Give the location of the input file 
loc = ("F:\\PROJECTS\\S1803_Script\\Script_Robin\\3411_Sched_PublicAreas-Hotel_Tower-042418.xlsx") 
  
# To open Workbook for reading
wb = xlrd.open_workbook(loc) 
print ("The number of worksheets is", wb.nsheets)

# To open workbook for writing and add sheet  
workbook = xlwt.Workbook()
w_sheet = workbook.add_sheet('test')
# create Header row [row,col]
w_sheet.write(0, 0, "Sheet Name")
w_sheet.write(0, 1, "Lighting")
#variable for storing row number in output sheet
w_row = 1

# loop for every sheet excel first 2 sheet and last sheet
for i in range(2,wb.nsheets-1): 
	# acces sheet using index number
    sheet = wb.sheet_by_index(i) 
    # For row 0 and column 0 
    sheet.cell_value(0, 0) 
    # Extracting number of rows 
    #print(sheet.name, ": ", sheet.nrows)
	# read name of sheet stored in cell (2,0)
    sheet_nm = sheet.cell_value(2,0)
	# for each row in sheet
    for rowidx in range(sheet.nrows):
		# access row using row index number
        row = sheet.row(rowidx)
		# enumerate row to check every cell
        for colidx, cell in enumerate(row):
			# if cell has 'Wattage per sqft' read its 10th col
            if cell.value == "Wattage per Sqft" :
                val = sheet.cell_value(rowidx, 10)
                print (sheet_nm, ": ", val )
				# write sheet name and value to output sheet
                w_sheet.write(w_row, 0, sheet_nm)
                w_sheet.write(w_row, 1, val)
                w_row = w_row + 1

#save output excel book
workbook.save('F:\\PROJECTS\\S1803_Script\\Script_Robin\\output.xls')